<?php

class SiteTests extends BaseTests
{
    /**
     * Testing that accessing the secure page doesn't works, and then logging
     */
    public function testAdmin()
    {
        $client = $this->createClient();

        // We are not admin, check that we get the error
        $crawler = $client->request('GET', '/addBook');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));

        // Logging in as admin, bad password
        $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'bad']);
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(0, $crawler->filter('.loginsuccess'));

        // Logging in as admin, success
        $crawler = $client->request('POST', '/admin', ['login' => 'admin', 'password' => 'password']);
        $this->assertCount(1, $crawler->filter('.loginsuccess'));

        // Now, we should get the page
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(0, $crawler->filter('.shouldbeadmin'));

        // Disconnect
        $crawler = $client->request('GET', '/logout');
        $this->assertTrue($client->getResponse()->isRedirect());
        $crawler = $client->request('GET', '/addBook');
        $this->assertCount(1, $crawler->filter('.shouldbeadmin'));
    }

    /**
     * Testing book insert (using form)
     */
    public function testBookInsertForm()
    {
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        // There is no book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(0, count($books));

        // Inserting one using a POST request through the form
        $client->request('GET', '/addBook');
        $form = $client->getCrawler()->filter('form')->form();
        $form['title'] = 'Test';
        $form['author'] = 'Someone';
        $form['synopsis'] = 'A test book';
        $form['copies'] = 3;
        $client->submit($form);

        // There is one book
        $books = $this->app['model']->getBooks();
        $this->assertEquals(1, count($books));
    }

    // Question 10
    public function testEmprunt()
    {
        $client = $this->createClient();
        $this->app['session']->set('admin', true);

        $books = $this->app['model']->insertBook('Test', 'Someone', 'A test book', 'image', 3);
        
        $exemplaires = $this->app['model']->getExemplaires();
        $this->assertEquals(3, count($books));

        // Inserting one using a POST request through the form
        $client->request('GET', '/emprunt/1');
        $form = $client->getCrawler()->filter('form')->form();
        $form['name'] = 'Test';
        $form['date_fin'] = date('Y-m-d H:i:s');
        $form['exemplaire'] = '1';
        $client->submit($form);

        $exemplaires = $this->app['model']->getExemplaires();
        $this->assertEquals(2, count($exemplaires));
    }
}




    /*public function testEmprunt()
    {

        // Inserting one using a POST request through the form
        $client->request('GET', '/addBook');
        $form = $client->getCrawler()->filter('form')->form();
        $form['title'] = 'Test';
        $form['author'] = 'Someone';
        $form['synopsis'] = 'A test book';
        $form['copies'] = 3;
        $client->submit($form);


        $books = $this->app['model']->getBooks();
        $this->assertTrue(1 <= count($books));

        $book = $books[0];

        $copies = $this->app['model']->getCopies($book['id']);
        // There are three copies of this book
        $this->assertTrue(3 == count($copies) );

        // There are three copies availables for borrow
        $this->assertTrue(3 == $this->app['model']->getAvailables($book['id']));

        //Borrow a book, assert that there are only two books left available
        $client->request('GET', '/borrow/' . $copies[0]['id']);
        $form = $client->getCrawler()->filter('form')->form();
        $form['name'] = 'Jean bon';
        $form['end'] = '2016-01-05 00:00:00';
        $client->submit($form);

        $this->assertTrue(2 == $this->app['model']->getAvailables($book['id']));

        // Return a borrowed book
        $this->app['model']->returnBook($copies[0]['id']);
        $this->assertTrue(3 == $this->app['model']->getAvailables($book['id']));
    }*/