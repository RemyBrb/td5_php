<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

        /**
     * Récupère un résultat exactement
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
        VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        //Question 4
        $lastIdBook = $this->pdo->lastInsertId();

        for($i=0; $i<$copies; $i++)
        {
            $exemplaires = $this->pdo->prepare('INSERT INTO exemplaires (book_id) VALUES (?)');
            $this->execute($exemplaires, array($lastIdBook));
        }
    }

    public function insertEmprunt($personne, $exemplaire, $date_debut, $date_fin)
    {
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin)
        VALUES (?, ?, ?, ?)');

        $this->execute($query, array($personne, $exemplaire, $date_debut, $date_fin));


    }

    /*public function getUnavailable($id)
    {
        $query = $this->pdo->prepare('SELECT emprunts.* FROM emprunts INNER JOIN exemplaires ON exemplaires.id = emprunts.exemplaire WHERE exemplaires.book_id = ?');

        $query->execute(array($id));

        return $query;   
    }*/
    // Question 6 
    public function getAvailable($id)
    {
        $query = $this->pdo->prepare('SELECT livres.titre, livres.image, livres.synopsis, exemplaires.book_id, exemplaires.id FROM livres INNER JOIN exemplaires ON livres.id = exemplaires.book_id WHERE exemplaires.id NOT IN (SELECT emprunts.exemplaire FROM emprunts WHERE emprunts.fini = 0) AND exemplaires.book_id = ?');
        $query->execute(array($id));

        return $query->fetchAll();        
    }

    public function getUnavailable($id)
    {
        $query = $this->pdo->prepare('SELECT livres.*,exemplaires.id as id_exemp, emprunts.fin, emprunts.fini, exemplaires.book_id FROM livres INNER JOIN exemplaires ON livres.id = exemplaires.book_id INNER JOIN emprunts ON exemplaires.id = emprunts.exemplaire WHERE exemplaires.book_id = ?');
        $query->execute(array($id));

        return $query;        
    }

    public function CancelEmprunt($id)
    {
        $query = $this->pdo->prepare('UPDATE emprunts SET fini = 1 WHERE emprunts.exemplaire = ?');
        $query->execute(array($id));

        return $query; 
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    public function getExemplaires()
    {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires');
        $this->execute($query);
        return $query->fetchAll();

    }

    public function getBook($id)
    {
        $sql = 'SELECT livres.* FROM livres WHERE livres.id = ?';

        $query = $this->pdo->prepare($sql);
        $query->execute(array($id));

        return $query;
    }
}
