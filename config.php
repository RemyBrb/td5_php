<?php

return [
    // Database
    'database' => [
        'engine' => 'mysql',
        'host' => 'localhost',
        'database' => 'td5',
        'user' => 'root',
        'password' => 'root'
    ],

    // Administrator auth
    // Question 2
    'admin' => [['admin', 'password'], ['root', 'root']]
];
